**Todos**

[ ] Make responsive

[ ] Fetch product from API

[ ] Write tests of routes, components and stores

# Fresh & organic

We are happy to provide fresh and organic products from Vietnam to you. You will find here the the cart's frontend, made upon Angular, only.

## Development

Install the project with all its dependencies to start developing.

```
git clone git@bitbucket.org:thomas_leperou/fresh-organic.git
cd fresh-organic
npm i
npm run start
```

And navigate to http://localhost:4200

## Main RFCs

The project is built up following this structure:

- **/routes** routable components, with its own module's space
- **/components** components distributed throughout all the project
- **/modals** routable components within a named outlet
- **/styles** add here the styles definition.

Styles have to be within their components and routes

Register your data within the Store. Currently, only the route `products` has a `store`.

## Contribute

Forks and PR are OK

## License

Apache License 2.0
