import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from 'src/app/routes/products/store/product.model';

interface Item {
  id: string,
  stock: number;
  name: string;
  icon: string;
  alt: string;
  label: string;
  price: number;
  unit: string;
  inCart: number;
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  @Input() isLoaded = false;
  @Input() items$: Observable<[Item]>;
  @Input() value$: Observable<Item>;

  @Output() selected: EventEmitter<Product> = new EventEmitter();
  @Output() more: EventEmitter<Product> = new EventEmitter();
  @Output() less: EventEmitter<Product> = new EventEmitter();

  printDescription(item: Item) {
    if (item.inCart <= 1) {
      return `Get 1 ${item.unit} for ${item.price}$`;
    }
    return `${(item.price * item.inCart).toFixed(2)}$ for ${item.inCart}${item.unit} (${item.price.toFixed(2)}/${item.unit})`;

  }

  selectItem(item: any) {
    this.selected.emit(item);
  }

  add(item: any): void {
    this.more.emit(item);
  }

  remove(item: any): void {
    this.less.emit(item);
  }

}
