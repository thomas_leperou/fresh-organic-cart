import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input() name: string | null = null;
  @Input() disabled = '';
  @Input() tabIndex = '';
  @Output() focused: EventEmitter<null> = new EventEmitter();

  hasFocused() {
    if (this.focused) {
      this.focused.emit(null);
    }
  }
}
