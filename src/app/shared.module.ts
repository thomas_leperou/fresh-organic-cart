import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';


import { SiteLogoComponent } from './components/site-logo/site-logo.component';
import { ListSuspenseComponent } from './components/list-suspense/list-suspense.component';
import { ListComponent } from './components/list/list.component';
import { ButtonComponent } from './components/button/button.component';
import { TagLineComponent } from './components/site-tag-line/tag-line.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
  ],
  declarations: [
    SiteLogoComponent,
    TagLineComponent,
    ButtonComponent,
    ListComponent,
    ListSuspenseComponent,
  ],
  exports: [
    CommonModule,
    BrowserModule,
    SiteLogoComponent,
    TagLineComponent,
    ButtonComponent,
    ListComponent,
    ListSuspenseComponent,
  ],
})
export class SharedModule {
}
