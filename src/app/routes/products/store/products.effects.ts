import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError, delay } from 'rxjs/operators';
import { ProductsService } from '../products.service';
import { LOAD_SUCCESS, LOAD } from './products.actions';

@Injectable()
export class ProductsEffects {

  public load$ = createEffect(
    () => this.actions$.pipe(
      ofType(LOAD),
      mergeMap(() => this.productsService.fetch()
        .pipe(
          delay(3000),  // fake network latency. bad 3g here
          map(({ products }: { products: string }) => {
            return ({ type: LOAD_SUCCESS, payload: products });
          }),
          catchError(() => EMPTY)
        ))
    )
  );

  constructor(
    private actions$: Actions,
    private productsService: ProductsService
  ) { }
}
