import { createAction, props } from '@ngrx/store';
import { Product } from './product.model';

export const LOAD = '[Products] Load';
export const LOAD_SUCCESS = '[Products] Load success';
export const ADD_TO_CART = '[Products] Add to cart';
export const REMOVE_FROM_CART = '[Products] Add remove from cart';

export const load = createAction(LOAD);
export const loadSucess = createAction(LOAD_SUCCESS, props<{ payload: Product[] }>());
export const addToCart = createAction(ADD_TO_CART, props<Product>());
export const removeFromCart = createAction(REMOVE_FROM_CART, props<Product>());
