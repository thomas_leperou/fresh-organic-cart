import { createSelector } from '@ngrx/store';
import { Product } from './product.model';


export const selectInCart = (state) => state.selectedUser;

export const selectVisibleBooks = createSelector(
  selectInCart,
  (products: Product[]) => {
    console.log('products are', products);
    return products;
  }
);
