export interface Product {
  id: string;
  label: string;
  name: string;
  unit: string;
  price: number;
  stock: number;
  inCart: number;
  icon?: string;
  picture?: string;
  alt?: string;
}


export class ProductModel implements Product {
  id = '';
  label = '';
  name = '';
  unit = '';
  price = 0;
  stock = 0;

  icon = '';
  picture = '';
  alt = '';

  inCart = 0;

  constructor(product) {
    //
    this.id = product.id || this.id;
    this.label = product.label || this.label;
    this.name = product.name || this.name;
    this.unit = product.unit || this.unit;
    this.price = product.price || this.price;
    this.stock = product.stock || this.stock;

    this.icon = product.icon || this.icon;
    this.picture = product.picture || this.picture;
    this.alt = product.alt || this.alt;

    this.inCart = product.inCart || 0;
  }
}
