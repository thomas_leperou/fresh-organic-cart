import { createReducer, on } from '@ngrx/store';
import { addToCart, load, loadSucess, removeFromCart } from './products.actions';
import { ProductModel as Product } from './product.model';

export const initialState: Product[] | [] = [];

const reducer = createReducer(initialState,
  // --
  // Add to cart
  on(addToCart, (state, product) => {
    const out = [];
    state.forEach((p: Product, i) => {
      if (p.id === product.id) {
        const inCart = p.stock === p.inCart ? p.inCart : p.inCart + 1;
        return out[i] = new Product({ ...p, inCart });
      }
      out[i] = new Product(p);
    });
    return out;
  }),

  // --
  // Remove from cart
  on(removeFromCart, (state, product) => {
    const out = [];
    state.forEach((p: Product, i) => {
      if (p.id === product.id) {
        const inCart = p.inCart === 0 ? 0 : p.inCart - 1;
        return out[i] = new Product({ ...p, inCart });
      }
      out[i] = new Product(p);
    });
    return out;
  }),

  // --
  // Load
  on(load, state => state),


  // --
  // Load sucess
  on(loadSucess, (_, { payload }) => {
    return payload.map(product => new Product(product));
  }),
);

export function productsReducer(state, action) {
  return reducer(state, action);
}
