import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store, select } from '@ngrx/store';

import { Product } from './store/product.model';
import { load, addToCart, removeFromCart } from './store/products.actions';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-products-route',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

  isLoaded = false;
  displayPicture = false;

  products$: Observable<Product[]>;
  activeProduct$: Observable<Product>;
  amount$: Observable<number>;

  constructor(
    private store: Store<{ products: Product[] }>,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    store.dispatch(load());

    this.initActiveProduct();
    this.initProducts();
    this.initAmount();

    // this.initDefaultProductOnRoute();
  }

  // --
  // Private

  private initActiveProduct() {
    this.activeProduct$ = this.route.params.pipe(
      map(p => p.product),
      switchMap((p) => this.findProduct(p))
    );
  }

  private initProducts() {
    this.products$ = this.store.pipe(select('products'));
    this.products$.subscribe((products) => this.isLoaded = products.length > 0);
  }

  private initAmount() {
    this.amount$ = this.products$.pipe(
      switchMap((p) => this.calculateCart(p))
    );
  }

  // This cause re-retouing even when
  // there is a product indicated in the URL
  // private initDefaultProductOnRoute() {
  //   this.activeProduct$.subscribe((product: Product) => {
  //     if (!product) {
  //       this.products$.subscribe((products) => {
  //         if (products[0]) {
  //           this.router.navigate([products[0]?.name]);
  //         }
  //       });
  //     }
  //   });
  // }

  private findProduct(name: string) {
    return this.products$.pipe(
      map((ps) => ps.find((p) => p.name === name))
    );
  }

  private calculateCart(products: Product[]) {
    let amount = 0;
    products.forEach((p: Product) => {
      amount += (p.price * p.inCart);
    });
    return of(amount);
  }

  // --
  // Public

  productSelected(product: Product) {
    this.router.navigate([product.name]);
  }

  addToCart(product: Product) {
    this.store.dispatch(addToCart(product));
  }

  removeFromCart(product: Product) {
    this.store.dispatch(removeFromCart(product));
  }

  navigateToCart() {
    this.router.navigate([{ outlets: { modal: ['cart'] } }]);
  }

}
