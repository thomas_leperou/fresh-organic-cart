import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { ProductsEffects } from './store/products.effects';
import { productsReducer } from './store/products.reducers';

import { ProductsComponent } from './products.component';
import { SharedModule } from 'src/app/shared.module';

const routes: Routes = [{
  path: '**',
  component: ProductsComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    StoreModule.forRoot({ products: productsReducer }),
    EffectsModule.forRoot([ProductsEffects]),
    SharedModule,
  ],
  declarations: [
    //
    ProductsComponent,
  ],
  exports: [RouterModule, SharedModule]
})
export class ProductsRoutingModule { }
