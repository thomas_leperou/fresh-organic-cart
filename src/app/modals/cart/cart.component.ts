import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { Product } from 'src/app/routes/products/store/product.model';
import { addToCart, removeFromCart } from 'src/app/routes/products/store/products.actions';
import { map, switchMap, delay } from 'rxjs/operators';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  isMounted = false;
  isLoaded = false;
  products$: Observable<Product[]>;
  amount$: Observable<number>;

  constructor(
    private store: Store<{ products: Product[] }>,
    private router: Router,
  ) {
    this.initProducts();
    this.initAmount();
  }

  ngOnInit() {
    // It sounds better to use the animation provided
    // by Angular, to animate properly the DOM elements
    // This hack does the job
    of(null).pipe(delay(1)).subscribe(() => this.isMounted = true);
  }

  // --
  // Private

  private initProducts() {
    this.products$ = this.store.pipe(
      select('products'),
      map((ps) => ps.filter((p) => p.inCart > 0))
    );

    this.products$.subscribe((products) => this.isLoaded = products?.length > 0);
  }

  private initAmount() {
    this.amount$ = this.products$.pipe(
      switchMap((p) => this.calculateCart(p))
    );
  }

  private calculateCart(products: Product[]) {
    let amount = 0;
    products.forEach((p: Product) => {
      amount += (p.price * p.inCart);
    });
    return of(amount);
  }

  // --
  // Public

  addToCart(product: Product) {
    this.store.dispatch(addToCart(product));
  }

  removeFromCart(product: Product) {
    this.store.dispatch(removeFromCart(product));
  }

  close() {
    // It sounds better to use the animation provided
    // by Angular, to animate properly the DOM elements
    // This hack does the job
    this.isMounted = false;
    of(null).pipe(delay(200)).subscribe(() => { this.router.navigate(['', { outlets: { modal: null } }]); });
  }

}
