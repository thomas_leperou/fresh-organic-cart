import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsComponent } from './routes/products/products.component';
import { CartComponent } from './modals/cart/cart.component';
import { SharedModule } from './shared.module';


const routes: Routes = [
  {
    path: ':product',
    component: ProductsComponent,
  },
  {
    outlet: 'modal',
    path: 'cart',
    component: CartComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
}),
  ],
  exports: [RouterModule, SharedModule]
})
export class AppRoutingModule { }
